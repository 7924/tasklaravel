					<div class="form-group">
						{!! Form::label('title', 'Title:') !!}
						{!! Form::text('title', null, ['class'=>'form-control']) !!}
					</div>
					<div class="form-group">
						{!! Form::label('body', 'Body:') !!}
						{!! Form::textarea('body', null, ['class'=>'form-control']) !!}
					</div>
					<div class="form-group">
						{!! Form::label('assigned_user', 'Assign To:') !!}
						{!! Form::select('assigned_user', [null=>'Please Select...']+$user, null, ['class'=>'form-control']) !!}
					</div>
					<div class="form-group">
						{!! Form::hidden('completed',0) !!}
						{!! Form::label('completed', 'Completed:') !!}
						{!! Form::checkbox('completed', 1, null,  ['class'=>'form-control']) !!}
					</div>
					<div class="form-group">
						{!! Form::submit($submitButtonText, ['class'=>'btn btn-primary form-control']) !!}
					</div>
					
