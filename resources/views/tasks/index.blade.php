@extends('app')

@section('content')

	<div class="panel-heading">Tasks</div>

	<div class="panel-body">
		<div>
		<a href="{{ action( 'TaskController@create') }}"><button type="button" class="btn btn-default btn-lg">Create new task</button></a>
		</div>
		<hr>
		@foreach($tasks as $task)


		{!! Form::open( ['method'=>'DELETE', 'route' => ['task.destroy', $task->id] ] ) !!}
		<a href="{{ action( 'TaskController@show', [$task->id]) }}">{{ $task->title }}</a>
		
		<div class="pull-right">
			<a href="{{ action( 'TaskController@show', [$task->id]) }}"><button type="button" class="btn btn-default btn-lg">Show</button></a>
			<a href="{{ action( 'TaskController@edit', [$task->id]) }}"><button type="button" class="btn btn-default btn-lg">Edit</button></a>

			{!! Form::submit('DELETE', ['class'=>'btn btn-default btn-lg'] ) !!}
		</div>
		{!! Form::close() !!}


		<hr>
		@endforeach
	</div>

@endsection
