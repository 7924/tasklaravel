@extends('app')

@section('content')

				<div class="panel-heading">Tasks</div>

				<div class="panel-body">
					<h2>Task title:{{ $task->title }}</h2>
					<p>Task body: {{ $task->body }}</p>
					<p>Task assigned To: {{$task->assignedUser['name']}}</p>
		
					{!! Form::open( ['method'=>'DELETE', 'route' => ['task.destroy', $task->id] ] ) !!}
					<div class="pull-right">
						<a href="{{ action( 'TaskController@edit', [$task->id]) }}"><button type="button" class="btn btn-default btn-lg">Edit</button></a>
						{!! Form::submit('DELETE', ['class'=>'btn btn-default btn-lg'] ) !!}
					</div>
					{!! Form::close() !!}
				</div>

@endsection
