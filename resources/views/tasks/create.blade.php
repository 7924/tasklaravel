@extends('app')

@section('content')

				<div class="panel-heading">Tasks</div>

				<div class="panel-body">
					<h2>Create new task</h2>
					{!! Form::open(['url'=>'task']) !!}
					
						@include('tasks._form', ['submitButtonText'=>'Add new Task'])

					{!! Form::close() !!}
					
					@include('errors.list')
				</div>

@endsection
