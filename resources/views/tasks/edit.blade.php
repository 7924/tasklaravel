@extends('app')

@section('content')

				<div class="panel-heading">Tasks</div>

				<div class="panel-body">
					<h2>Edit: {{$task->title}}</h2>
					{!! Form::model($task, ['method'=>'PATCH', 'action'=>['TaskController@update', $task->id]  ]) !!}
						@include('tasks._form', ['submitButtonText'=>'Edit Task'])
					{!! Form::close() !!}
					
					@include('errors.list')
				</div>

@endsection
