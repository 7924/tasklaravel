Hi, {{ $author['name'] }}

Your task "{{ $task['title'] }}" is completed.

Task was assigned to: {{ $assignedUser['name'] }}.

