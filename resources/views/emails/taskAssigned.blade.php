Hi, {{ $assignedUser['name'] }}

New Task "{{ $task['title'] }}" is assigned to You!

Task created by {{ $author['name'] }}.
