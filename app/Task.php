<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model {

	protected $fillable = [
		'title',
		'body',
		'author_id',
		'assigned_user',
		'completed'
	];
	
	public function User(){
		return $this->belongsTo('App\User', 'author_id');
	}
	
	public function assignedUser(){
		return $this->belongsTo('App\User', 'assigned_user');
	}

}
