<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Task;
use Request;
use Auth;
//use App\Http\Requests\TaskRequest;
use TaskRequest;
use Mail;

class TaskController extends Controller {

	public function __construct() {
		$this->middleware('auth');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index() {
		$tasks = Task::with(['User', 'assignedUser'])->get();
		return view('tasks.index', compact('tasks'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create() {
		$user = \App\User::all()->lists('name', 'id');

		return view('tasks.create', compact('user'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Requests\TaskRequest $request) {

		$input = Request::all();
		$input['author_id'] = Auth::user()->id;
		$task = Task::create($input);
		if ($task->assigned_user) {
			$data['assignedUser'] = $task->assignedUser->toArray();
			$data['author'] = $task->user->toArray();
			$data['task'] = $task->toArray();
			
			Mail::send('emails.taskAssigned', $data, function($message) use ($data) {
				$message->from('tra@tra.tra', 'tra-ta-ta');
				$message->to($data['assignedUser']['email'], $data['assignedUser']['name'])->subject('NewTask!');
			});
		}
		
		if($task->completed == 1){
			// completed status is set true
			if(is_object($task->assignedUser)){
				$data['assignedUser'] =   $task->assignedUser->toArray();
			} else {
				$data['assignedUser']['name'] = 'Assigned user is not set';
			}
			$data['author'] = $task->user->toArray();
			$data['task'] = $task->toArray();
			
			Mail::send('emails.taskCompleted', $data, function($message) use ($data) {
				$message->from('tra@tra.tra', 'tra-ta-ta');
				$message->to($data['author']['email'], $data['author']['name'])->subject('NewTask!');
			});
		}		

		return redirect(url('task'));
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id) {
		$task = Task::with('assignedUser')->findOrFail($id);
		return view('tasks.show', compact('task'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id) {
		$task = Task::findOrFail($id);
		$user = \App\User::all()->lists('name', 'id');
		return view('tasks.edit', compact(['task', 'user']));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, Requests\TaskRequest $request) {
		$task = Task::findOrFail($id);
		$oldCompleted = $task->completed;
		$oldAssignedUser = $task->assigned_user;
		
		$task->update($request->all());

		//dd($task->assigned_user=='');
		if( $oldAssignedUser != $task->assigned_user && $task->assigned_user != '') {
			// assigned user is changed
			$data['assignedUser'] = $task->assignedUser->toArray();
			$data['author'] = $task->user->toArray();
			$data['task'] = $task->toArray();
			
			Mail::send('emails.taskAssigned', $data, function($message) use ($data) {
				$message->from('tra@tra.tra', 'tra-ta-ta');
				$message->to($data['assignedUser']['email'], $data['assignedUser']['name'])->subject('NewTask!');
			});
		}
		

		if($oldCompleted != $task->completed && $task->completed == 1){
			// completed status is changed
			if(is_object($task->assignedUser)){
				$data['assignedUser'] =   $task->assignedUser->toArray();
			} else {
				$data['assignedUser']['name'] = 'Assigned user is not set';
			}
			$data['author'] = $task->user->toArray();
			$data['task'] = $task->toArray();
			
			Mail::send('emails.taskCompleted', $data, function($message) use ($data) {
				$message->from('tra@tra.tra', 'tra-ta-ta');
				$message->to($data['author']['email'], $data['author']['name'])->subject('NewTask!');
			});
		}

		return redirect('task');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id) {
		$task = Task::findOrFail($id);
		$task->delete();
		return redirect('task');
	}

}
